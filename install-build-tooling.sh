#!/usr/bin/env bash

set -xe

apt-get -y install \
  clang \
  cmake \
  libclang-dev \
  llvm-dev
