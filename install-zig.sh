#!/usr/bin/env bash

set -xe

VERSION=${1:-'0.11.0'}
DIR=${2:-'/opt'}
machine=$(uname -m)

case $machine in
  x86_64)
    ARCH=x86_64
    ;;

  aarch64)
    ARCH=aarch64
    ;;

  armv7l)
    ARCH=armv7a
    ;;
  *)
    echo "Invalid machine architecture $machine"
    exit 1
esac

mkdir -p $DIR

curl --proto '=https' --tlsv1.2 -sSfL \
  "https://ziglang.org/download/${VERSION}/zig-linux-${ARCH}-${VERSION}.tar.xz" \
  -o $DIR/zig.tar.xz

tar -xJf $DIR/zig.tar.xz
rm $DIR/zig.tar.xz

mv zig-linux-${ARCH}-${VERSION} $DIR/zig
