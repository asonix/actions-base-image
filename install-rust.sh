#!/usr/bin/env bash

set -xe

TOOLCHAIN=${1:-'stable'}

mkdir -p /opt 

curl --proto '=https' --tlsv1.2 -sSfL 'https://sh.rustup.rs' -o /opt/rustup.sh 
chmod +x /opt/rustup.sh 
/opt/rustup.sh --profile minimal --default-toolchain $TOOLCHAIN -y 
rm /opt/rustup.sh 

curl --proto '=https' --tlsv1.2 -sSfL 'https://raw.githubusercontent.com/cargo-bins/cargo-binstall/main/install-from-binstall-release.sh' -o /opt/cargo-binstall.sh
chmod +x /opt/cargo-binstall.sh
/opt/cargo-binstall.sh
rm /opt/cargo-binstall.sh


rustup component add clippy 
rustup target add x86_64-unknown-linux-musl 
rustup target add armv7-unknown-linux-musleabihf
rustup target add aarch64-unknown-linux-musl 

yes | cargo binstall bindgen-cli cargo-deny cargo-zigbuild
