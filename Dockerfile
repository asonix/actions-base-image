FROM docker.io/node:20-bookworm

RUN \
  mkdir -p /opt && \
  apt-get update && \
  apt-get install -y jq

ENV \
  PATH=/opt/minio-client:/opt/zig:/root/.cargo/bin:/root/.rustup/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin

COPY --chmod=744 install-build-tooling.sh /opt/install-build-tooling.sh
RUN bash /opt/install-build-tooling.sh

COPY --chmod=744 install-docker.sh /opt/install-docker.sh
RUN bash /opt/install-docker.sh

COPY --chmod=744 install-zig.sh /opt/install-zig.sh
RUN bash /opt/install-zig.sh

COPY --chmod=744 install-rust.sh /opt/install-rust.sh
RUN bash /opt/install-rust.sh

COPY --chmod=744 install-minio-client.sh /opt/install-minio-client.sh
RUN bash /opt/install-minio-client.sh

CMD ["/usr/bin/bash"]
