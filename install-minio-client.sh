#!/usr/bin/env bash

set -xe

DIR=${1:-'/opt/minio-client'}
machine=$(uname -m)

case $machine in
  x86_64)
    ARCH=amd64
    ;;
  aarch64)
    ARCH=arm64
    ;;
  armv7l)
    ARCH=arm
    ;;
  *)
    echo "Unsupported architecture $arch";
    exit 1;
    ;;
esac

mkdir -p $DIR

curl --proto '=https' --tlsv1.2 -sSfL \
  "https://dl.min.io/client/mc/release/linux-${ARCH}/mc" \
  -o $DIR/mc

chmod +x $DIR/mc
